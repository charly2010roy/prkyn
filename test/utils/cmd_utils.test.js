const test = require('ava');
const objectifyCmd = require('../../src/utils/cmd_utils');
const {
  CREATE_PARKING_LOT,
  PARK,
  LEAVE,
  STATUS,
} = require('../../src/utils/consts');


test('objectifyCmd', (t) => {
  t.deepEqual(objectifyCmd(`${LEAVE} XB-09 2`), {
    cmd: LEAVE,
    regn: 'XB-09',
    duration: '2',
  });
  t.deepEqual(objectifyCmd(`${STATUS}`), {
    cmd: STATUS,
  });
  t.deepEqual(objectifyCmd(`${PARK} XB-09`), {
    cmd: PARK,
    regn: 'XB-09',
  });
  t.deepEqual(objectifyCmd(`${CREATE_PARKING_LOT} 9`), {
    cmd: CREATE_PARKING_LOT,
    size: '9',
  });
});
