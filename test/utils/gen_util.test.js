const test = require('ava');
const utils = require('../../src/utils/gen_utils');
const Car = require('../../src/classes/car');
const { vehicleType } = require('../../src/utils/consts');

test('genArr', (t) => {
  t.deepEqual(utils.genArr(5), [1, 2, 3, 4, 5]);
});

test('vehiclefactory', (t) => {
  const x = utils.vehicleFactory('FR', vehicleType.CAR);
  t.truthy(x instanceof Car);

  const y = utils.vehicleFactory('XC', 'truck');
  t.is(y, null);
});
