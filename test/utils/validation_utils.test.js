const test = require('ava');
const utils = require('../../src/utils/validation_utils');

const {
  validateCmd,
  compose,
  __utils,
} = utils;

test('validateCmd', (t) => {
  t.is(validateCmd('status'), 'status');
  t.is(validateCmd('   create_parking_lot 9'), 'create_parking_lot 9');
  t.is(validateCmd('park    KA-02'), 'park KA-02');
  t.is(validateCmd('leave KA-02  5'), 'leave KA-02 5');

  t.is(validateCmd('leave KA-02 '), '');
  t.is(validateCmd('   create_parking_lot A'), '');
  t.is(validateCmd('leave ts-06'), '');
  t.is(validateCmd('park'), '');
  t.is(validateCmd('leave'), '');
  t.is(validateCmd('status 78'), '');
  t.is(validateCmd('close'), '');
  t.is(validateCmd('create_parking_lot 4444'), '');
});

test('compose', (t) => {
  const f0 = (int) => int * 2;
  const f1 = (int) => int * int;
  t.is(compose(f0, f1)(5), 50);
  t.is(compose(f1, f0)(5), 100);
  t.is(compose(f1, f0, f1)(10), 40000);
});

test('trimSpace', (t) => {
  t.is(__utils.trimSpace('abc'), 'abc');
  t.is(__utils.trimSpace('abc   '), 'abc');
  t.is(__utils.trimSpace('a bc'), 'a bc');
  t.is(__utils.trimSpace('a  bc'), 'a bc');
  t.is(__utils.trimSpace('   a b c   '), 'a b c');
  t.is(__utils.trimSpace('  ab  c  '), 'ab c');
});

test('validate', (t) => {
  t.is(__utils.validate('status'), 'status');
  t.is(__utils.validate('create_parking_lot 9'), 'create_parking_lot 9');
  t.is(__utils.validate('park KA-02'), 'park KA-02');
  t.is(__utils.validate('leave KA-02 5'), 'leave KA-02 5');

  t.is(__utils.validate(' status'), '');
  t.is(__utils.validate('create_parking_lot    9'), '');
  t.is(__utils.validate('park KA-02   '), '');
  t.is(__utils.validate('    leave KA-02 5'), '');
});
