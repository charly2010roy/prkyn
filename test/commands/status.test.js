const test = require('ava');
const Parking = require('../../src/classes/parking');
const status = require('../../src/commands/status');
const {
  vehicleType,
} = require('../../src/utils/consts');

test.before(() => {
  Parking.createParking(5);
});

test.serial('status in empty lot', async (t) => {
  t.falsy(await status());
});

test.serial('status in non-empty lot', async (t) => {
  Parking.park('AWD', vehicleType.CAR);
  Parking.park('FWD', vehicleType.CAR);
  Parking.park('RWD', vehicleType.CAR);
  const result = await status();
  const rows = result.split('\n');
  t.is(rows.length, 4);
});
