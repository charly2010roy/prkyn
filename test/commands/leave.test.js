const test = require('ava');
const Parking = require('../../src/classes/parking');
const leave = require('../../src/commands/leave');
const park = require('../../src/commands/park');

test.before(() => {
  Parking.createParking(5);
});

test.serial('remove non existing car', async (t) => {
  const res = await leave({
    regn: 'AWS-8',
    duration: 8,
  });
  t.is(res, 'Registration number AWS-8 not found');
});

test.serial('remove a car', async (t) => {
  await park({
    regn: 'ABB-9',
  });
  const res = await leave({
    regn: 'ABB-9',
    duration: 4,
  });
  t.is(res, 'Registration number ABB-9 with Slot Number 1 is free with Charge 30');
});
