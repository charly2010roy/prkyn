const test = require('ava');
const Parking = require('../../src/classes/parking');
const park = require('../../src/commands/park');

test.before(() => {
  Parking.createParking(2);
});

test.serial('park a car', async (t) => {
  const res = await park({
    regn: 'KA-56',
  });
  t.is(res, 'Allocated slot number: 1');
});

test.serial('park a car again', async (t) => {
  await t.throwsAsync(() => park({
    regn: 'KA-56',
  }));
});

test.serial('park in a full lot', async (t) => {
  await park({
    regn: 'WB-67',
  });
  const res = await park({
    regn: 'AP-33',
  });
  t.is(res, 'Sorry, parking lot is full');
});
