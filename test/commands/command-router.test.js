const test = require('ava');
const routeCommand = require('../../src/commands/command_router');

const funcs = {
  leave: () => 3,
  status: () => 7,
};

test('route commands', async (t) => {
  t.is(await routeCommand('leave', funcs), 3);
  t.is(await routeCommand('status', funcs), 7);
});
