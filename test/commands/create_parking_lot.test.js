const test = require('ava');
const cpl = require('../../src/commands/create_parking_lot');

test.serial('create parking lot', async (t) => {
  const res = await cpl({
    size: 5,
  });
  t.is(res, 'Created parking lot with 5 slots');
});

test.serial('create parking lot again errors', async (t) => {
  await t.throwsAsync(() => cpl({
    size: 7,
  }));
});
