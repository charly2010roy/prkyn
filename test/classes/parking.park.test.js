const test = require('ava');
const Car = require('../../src/classes/car');
const Ticket = require('../../src/classes/ticket');
const Parking = require('../../src/classes/parking');
const { vehicleType } = require('../../src/utils/consts');


test.before(() => {
  Parking.createParking(2);
});

test('park a vehicle', (t) => {
  t.is(Parking.park('CAR-0', vehicleType.CAR), 1);

  // same car can't be parked
  t.throws(() => {
    Parking.park('CAR-0', vehicleType.CAR);
  });

  t.is(Parking.park('CAR-1', vehicleType.CAR), 2);

  // parking full
  t.is(Parking.park('CAR-2', vehicleType.CAR), -1);

  const spots = Parking.getAllSpotsFromParking();
  const filledSpots = spots.filter((spot) => spot.isSpotBooked()).length;
  t.is(filledSpots, 2);

  const carAtSpot = spots[0].getVehicleAtSpot();
  t.truthy(carAtSpot instanceof Car);

  const ticketInCarAtSpot = carAtSpot.getTicketFromVehicle();
  t.truthy(ticketInCarAtSpot instanceof Ticket);
});
