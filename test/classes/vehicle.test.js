const test = require('ava');
const Vehicle = require('../../src/classes/vehicle');

test('Is Abstract', (t) => {
  t.throws(() => {
    new Vehicle('regn', 'pink', 'train');
  });
});
