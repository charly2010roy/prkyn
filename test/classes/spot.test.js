const test = require('ava');
const Spot = require('../../src/classes/spot');

test.serial('spot methods 1', (t) => {
  const spot = new Spot(1, 1);
  t.is(spot.isSpotBooked(), false);
  t.is(spot.getIdOfSpot(), 1);
});

test.serial('spot methods 2', (t) => {
  const spot = new Spot(2, 2);
  t.is(spot.isSpotBooked(), false);
  t.falsy(spot.getVehicleAtSpot());

  spot.assignVehicleToSpot('XT');

  t.is(spot.isSpotBooked(), true);
  t.is(spot.getVehicleAtSpot(), 'XT');

  spot.removeVehicleFromSpot();

  t.is(spot.isSpotBooked(), false);
  t.is(spot.getVehicleAtSpot(), null);
});
