const test = require('ava');
const Parking = require('../../src/classes/parking');
const { vehicleType } = require('../../src/utils/consts');


test.before(() => {
  Parking.createParking(2);
});

test.serial('unpark non-existing vehicle', (t) => {
  const result = Parking.leave('XB7', 7);
  t.is(result, null);
});

test.serial('unpark a vehicle', (t) => {
  Parking.park('XB7', vehicleType.CAR);
  const result = Parking.leave('XB7', 8);
  t.truthy(result);
  t.is(result.regn, 'XB7');
  t.is(result.spotId, 1);
  t.is(result.bill, 70);
});
