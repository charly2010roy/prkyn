const test = require('ava');
const Spot = require('../../src/classes/spot');
const Parking = require('../../src/classes/parking');

test.before(() => {
  Parking.createParking(10);
});

test.serial('Can be newed only once', (t) => {
  const spots = Parking.getAllSpotsFromParking();
  t.is(spots.length, 10);
  t.is(Parking.getSizeOfParking(), 10);

  t.throws(() => {
    Parking.createParking(20);
  });
  const spotsNow = Parking.getAllSpotsFromParking();
  t.is(spotsNow.length, 10);
  t.is(Parking.getSizeOfParking(), 10);
});

test.serial('Empty spots created', (t) => {
  const spots = Parking.getAllSpotsFromParking();
  t.truthy(spots[0] instanceof Spot);
});
