const test = require('ava');
const Car = require('../../src/classes/car');
const Vehicle = require('../../src/classes/vehicle');

test('car methods', (t) => {
  const car = new Car('AB-56');
  t.is(car.getRegnOfVehicle(), 'AB-56');
  t.is(car.getTicketFromVehicle(), null);

  car.assignTicketToVehicle('Ticket');
  t.is(car.getTicketFromVehicle(), 'Ticket');
});

test('car inheritance', (t) => {
  const car = new Car('XB-89');
  t.truthy(car instanceof Car);
  t.truthy(car instanceof Vehicle);
});
