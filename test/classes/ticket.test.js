const test = require('ava');
const Ticket = require('../../src/classes/ticket');

test('ticket methods', (t) => {
  const ticket = new Ticket();
  t.is(ticket.getDuration(), 0);
  t.is(ticket.calcCost(), 10);

  ticket.checkout(5);

  t.is(ticket.getDuration(), 5);
  t.is(ticket.calcCost(), 40);

  ticket.checkout(1);

  t.is(ticket.getDuration(), 1);
  t.is(ticket.calcCost(), 10);
});
