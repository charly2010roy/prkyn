const psh = require('./psh');
const pfile = require('./pfile');

module.exports = {
  run: () => {
    const { argv } = process;

    if (argv.length === 3 && argv[2].endsWith('.txt')) {
      pfile();
    }
    if (argv.length === 2) {
      psh();
    }
    return () => {
      console.log('Unknown Mode');
    };
  },
};
