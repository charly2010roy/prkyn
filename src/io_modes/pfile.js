const fs = require('fs');
const readline = require('readline');
const exec = require('../utils/exec');

/**
 * handles readline for file input
 */
const pfile = async () => {
  const rl = readline.createInterface({
    input: fs.createReadStream(process.argv[2]),
  });

  for await (const line of rl) {
    exec(line);
  }

  rl.on('close', () => {
    process.exit(0);
  });
};

module.exports = pfile;
