const readline = require('readline');
const exec = require('../utils/exec');

/**
 * handles readline for cli input
 */
const psh = async () => {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    prompt: 'psh> ',
  });

  rl.prompt();

  for await (const line of rl) {
    exec(line);
    rl.prompt();
  }

  rl.on('close', () => {
    process.exit(0);
  });
};

module.exports = psh;
