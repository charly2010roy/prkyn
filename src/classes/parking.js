const Spot = require('./spot');
const Ticket = require('./ticket');
const {
    vehicleFactory,
    genArr
} = require('../utils/gen_utils');

class Parking {
    // array of spots
    static spots;

    static maxSize;
    
    /**
     * maps regn of vehicle to id of spot
     * to improve spot search during unparking
     * and checking list of cars 
     */
    static vehicleSpotMap;

    /**
     * Represents a parking lot. Initiated once per app.
     * @constructor
     * @param {number} maxSize - size of lot
     */
    constructor(maxSize) {
        if (Parking.maxSize) {
            throw Error('Can only create a parking lot once');
        }
        Parking.maxSize = Parking.maxSize || maxSize;
        // generates array [1,2,3.....n] and ctreates those many spots
        Parking.spots = Parking.spots || (genArr(Parking.maxSize)).map(e => new Spot(e, e));
        Parking.vehicleSpotMap = Parking.vehicleSpotMap || {};
    }

    /**
     * Calls constructor
     * @method
     * @param {maxSize} number
     */
    static createParking(maxSize) {
        maxSize && new Parking(maxSize);
    }

    /**
     * Get all spots of the lot
     * @method
     * @returns {object} array of parking spots
     */
    static getAllSpotsFromParking() {
        return Parking.spots;
    }

    /**
     * Get size of the lot
     * @method
     * @returns {number} size
     */
    static getSizeOfParking() {
        return Parking.spots.length;
    }

    /**
     * park a vehicle at the closest open spot
     * @method
     * @param {string} regn
     * @param {string} vehicleType
     * @returns {number} id of the available spot or -1 if fully booked
     * @throws {object} error object
     */
    static park(regn, vehicleType) {
        
        // check if vehicle is present in the lot
        if (Parking.vehicleSpotMap[regn]) {
            throw Error('Already parked vehicle');
        }

        // create vehicle + ticket
        const ticket = new Ticket();
        const vehicle = vehicleFactory(regn, vehicleType);
        vehicle.assignTicketToVehicle(ticket);

        // find empty spot
        let index = -1;
        index = Parking
            .spots
            .findIndex(spot => !spot.isSpotBooked());

        // return -1 if no spots are empty
        if (index === -1) {
            return index;
        } else {
            Parking.spots[index].assignVehicleToSpot(vehicle);
            const spotId = Parking.getAllSpotsFromParking()[index].getIdOfSpot();
            Parking.vehicleSpotMap[regn] = spotId;
            return spotId;
        }
    }
/**
     * remove vehicle from lot
     * @method
     * @param {string} regn
     * @param {number} duration hours stayed
     * @returns {object} null if vehicle not in lot
     */
    static leave(regn, duration) {

        // check if vehicle is not present in the lot
        if (!Parking.vehicleSpotMap[regn]) {
            return null;
        }

        const spotId = Parking.vehicleSpotMap[regn];
        const index = spotId - 1;

        const spot = Parking.spots[index];
        const freeVehicle = spot.getVehicleAtSpot();
        const ticket = freeVehicle.getTicketFromVehicle();

        // detatch vehicle from spot
        spot.removeVehicleFromSpot();
        ticket.checkout(duration);
        
        const bill = ticket.calcCost();

        // delete vehicle-spot key-value from map
        delete Parking.vehicleSpotMap[regn];
        return {
            regn,
            bill,
            spotId
        }
    }
}

module.exports = Parking;