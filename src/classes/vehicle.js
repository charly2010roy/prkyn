/**
 * Vehicle is a less specific class of car
 * Can put common behaviours in it
 */
class Vehicle {
  /**
     * Represents a vehicle.
     * @constructor
     * @param {string} regn - registration number
     * @param {string} color
     * @param {string} type
     */
  constructor(regn, color, type) {
    if (new.target === Vehicle) {
      throw Error('Meant to be an abstract class');
    }
    this.regn = regn;
    this.color = color;
    this.type = type;
    this.ticket = null;
  }

  /**
     * Attach ticket to a vehicle
     * @method
     * @param {object} ticket - ticket object
     */
  assignTicketToVehicle(ticket) {
    this.ticket = ticket;
  }

  /**
     * Get ticket attached to a vehicle
     * @method
     * @returns {object} ticket object
     */
  getTicketFromVehicle() {
    return this.ticket;
  }

  /**
     * Get registration number of a vehicle
     * @method
     * @returns {string}
     */
  getRegnOfVehicle() {
    return this.regn;
  }
}

module.exports = Vehicle;
