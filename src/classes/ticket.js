class Ticket {
  /**
     * Represents a ticket
     * @constructor
     */
  constructor() {
    this.duration = 0;
    this.billing = 'simple';
  }

  /**
     * Upadte duration on a ticket
     * @method
     * @param {number} hours - hours parked
     */
  checkout(after) {
    this.duration = after;
  }

  /**
     * Get duration from ticket
     * @method
     * @returns {number} - hours parked
     */
  getDuration() {
    return this.duration;
  }

  /**
     * Calculate cost on checkout
     * @method
     * @returns {number} - money
     */
  calcCost() {
    if (this.billing === 'simple') {
      return this.simple();
    }
    return 0;
  }

  /**
     * Simple algo to calculate cost
     * @method
     * @returns {number} - money
     */
  simple() {
    const baseFee = 10;
    const recurFee = 10;
    const extraHours = this.getDuration() - 2;
    return extraHours <= 0 ? baseFee : baseFee + extraHours * recurFee;
  }
}

module.exports = Ticket;
