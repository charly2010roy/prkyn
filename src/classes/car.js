const Vehicle = require('./vehicle');
const { vehicleType } = require('../utils/consts');

class Car extends Vehicle {
  /**
     * Represents a car.
     * @constructor
     * @param {string} regn - registration
     * @param {string} color - default is black
     */
  constructor(regn, color = 'black') {
    super(regn, color, vehicleType.CAR);
  }
}

module.exports = Car;
