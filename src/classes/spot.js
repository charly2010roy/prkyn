/**
 * Spot is already quite generic
 * More specific spot types can extend it
 */
class Spot {
  /**
     * Represents a parking spot.
     * @constructor
     * @param {string} id - ID of parking spot
     * @param {number} distance - distance from entrance
     * @param {boolean} booked - default is false/empty
     */
  constructor(id, distance, booked = false) {
    this.id = id;
    this.distance = distance;
    this.booked = booked;
    this.vehicle = null;
  }

  /**
     * Assign a vehicle to a parking spot
     * @method
     * @param {object} vehicle - vehicle object
     */
  assignVehicleToSpot(vehicle) {
    this.vehicle = vehicle;
    this.booked = true;
  }

  /**
     * Remove vehicle from a parking spot
     * @method
     */
  removeVehicleFromSpot() {
    this.vehicle = null;
    this.booked = false;
  }

  /**
     * Get vehicle at a parking spot
     * @method
     * @returns {object} vehicle object
     */
  getVehicleAtSpot() {
    return this.vehicle;
  }

  /**
     * Get status of a parking spot
     * @method
     * @returns {boolean}
     */
  isSpotBooked() {
    return this.booked;
  }

  /**
     * Get ID of a parking spot
     * @method
     * @returns {number}
     */
  getIdOfSpot() {
    return this.id;
  }
}

module.exports = Spot;
