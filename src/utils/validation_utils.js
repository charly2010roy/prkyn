const {
  CREATE_PARKING_LOT,
  LEAVE,
  PARK,
  STATUS,
} = require('./consts');

/**
 * Removes spaces from the end and extra spaces between words
 * @param {string} str
 * @returns {string}
 */
const trimSpace = (str) => {
  const trimmedStr = str.trim().replace(/\s+/g, ' ');
  return trimmedStr;
};

/**
 * Allowed format of commands
 */
const getPatterns = () => [
  `^${CREATE_PARKING_LOT} [0-9]{1,3}$`,
  `^${STATUS}$`,
  `^${LEAVE} [A-Z0-9-]{1,} [0-9]{1,}$`,
  `^${PARK} [A-Z0-9-]{1,}$`,
];

/**
 * validates the command against the list of regex
 * @param {string} str
 * @returns {string} '' if invalid cmd + args
 */
const validate = (str) => {
  const regs = getPatterns();
  for (let i = 0; i < regs.length; i += 1) {
    if ((new RegExp(regs[i])).test(str)) {
      return str;
    }
  }
  return '';
};

// compose(a,b,c) returns a(b(c(x)))
const compose = (...funcs) => (x) => funcs.reduceRight((y, func) => func(y), x);

// first trims spaces and then validates with regex
const validateCmd = compose(validate, trimSpace);

const __utils = {
  trimSpace,
  validate,
};

module.exports = {
  validateCmd,
  compose,
  __utils,
};
