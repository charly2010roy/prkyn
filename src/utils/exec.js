const { validateCmd } = require('./validation_utils');
const cmdr = require('../commands/command_router');

/**
 * handles over cmd + args string to router
 * after string is cleaned and validated
 * @param {string} line - cmd + args
 * @param {function} execFunc - function to execute
 * @param {function} validateFunc - validation function
 */
const exec = async (line, execFunc = cmdr, validateFunc = validateCmd) => {
  const validated = validateFunc(line.toString());
  if (validated === '') {
    console.log('Invalid');
  } else {
    try {
      const result = await execFunc(validated);
      result && console.log(result);
    } catch (e) {
      process.env.PRKYN_ENV === 'test' && console.log(e);
    }
  }
};

module.exports = exec;
