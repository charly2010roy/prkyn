const Car = require('../classes/car');
const { vehicleType } = require('./consts');

const genArr = (n) => [...Array(n)].map((_, index) => index + 1);

const vehicleFactory = (regn, type) => {
  if (type === vehicleType.CAR) {
    return new Car(regn);
  }
  return null;
};

module.exports = {
  genArr,
  vehicleFactory,
};
