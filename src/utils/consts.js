const CREATE_PARKING_LOT = 'create_parking_lot';
const LEAVE = 'leave';
const STATUS = 'status';
const PARK = 'park';

const vehicleType = {
  CAR: 'car',
};

const sorryFull = 'Sorry, parking lot is full';
const slotAllocated = 'Allocated slot number:';

module.exports = {
  CREATE_PARKING_LOT,
  LEAVE,
  STATUS,
  PARK,
  vehicleType,
  sorryFull,
  slotAllocated,
};
