const {
  CREATE_PARKING_LOT,
  PARK,
  LEAVE,
  STATUS,
} = require('./consts');

const cmdObjStrategies = {};
cmdObjStrategies[CREATE_PARKING_LOT] = (cargs) => ({
  cmd: cargs[0],
  size: cargs[1],
});
cmdObjStrategies[PARK] = (cargs) => ({
  cmd: cargs[0],
  regn: cargs[1],
});
cmdObjStrategies[STATUS] = (cargs) => ({
  cmd: cargs[0],
});
cmdObjStrategies[LEAVE] = (cargs) => ({
  cmd: cargs[0],
  regn: cargs[1],
  duration: cargs[2],
});

/**
 * Converts a command + args string to an object using
 * the conversion technique inferred from the command
 * @param {string} str
 * @param {object} strategies
 */
const objectifyCmd = (str, strategies = cmdObjStrategies) => {
  const cargs = str.split(' ');
  const cmd = cargs[0];
  return strategies[cmd](cargs);
};

module.exports = objectifyCmd;
