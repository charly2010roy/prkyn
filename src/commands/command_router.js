const cpl = require('./create_parking_lot');
const leave = require('./leave');
const park = require('./park');
const status = require('./status');
const objectifyCmd = require('../utils/cmd_utils');

const {
  CREATE_PARKING_LOT,
  PARK,
  LEAVE,
  STATUS,
} = require('../utils/consts');

/**
 * A collection of functions
 * Each function handles one command
 */
const execStrategies = {};
execStrategies[CREATE_PARKING_LOT] = cpl;
execStrategies[PARK] = park;
execStrategies[STATUS] = status;
execStrategies[LEAVE] = leave;

/**
 * handles over responsibility to handlers
 * @method
 * @param {str} command - command + args string
 * @param {object} strategies
 * @returns {object} output of handler
 */
const routeCommand = async (str, strategies = execStrategies) => {
  const cmdObj = objectifyCmd(str);
  const { cmd } = cmdObj;
  return strategies[cmd](cmdObj);
};

module.exports = routeCommand;
