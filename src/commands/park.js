const Parking = require('../classes/parking');
const {
  vehicleType,
  sorryFull,
  slotAllocated,
} = require('../utils/consts');


/**
 * handles park command
 * @param {*} cmdObj
 */
const park = async (cmdObj) => {
  const { regn } = cmdObj;
  try {
    const spotId = Parking.park(regn, vehicleType.CAR);
    return spotId === -1 ? sorryFull : `${slotAllocated} ${spotId}`;
  } catch (e) {
    throw e;
  }
};

module.exports = park;
