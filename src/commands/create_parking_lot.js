const Parking = require('../classes/parking');

/**
 * handles create_parking_lot command
 * @param {*} cmdObj
 */
const cpl = async (cmdObj) => {
  try {
    Parking.createParking(parseInt(cmdObj.size));
    return `Created parking lot with ${Parking.getSizeOfParking()} slots`;
  } catch (e) {
    throw (e);
  }
};

module.exports = cpl;
