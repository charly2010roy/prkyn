const Parking = require('../classes/parking');

/**
 * handles status command
 * @param {*} cmdObj
 */
const status = async () => {
  const spots = Parking.getAllSpotsFromParking();
  const bookedSpots = spots && spots.filter((spot) => spot.isSpotBooked());

  // formatting output data to match expected output
  if (bookedSpots && bookedSpots.length > 0) {
    const header = 'Slot No.    Registration No.';
    const spacer = '           ';
    const data = [header];
    const tmpArr = bookedSpots
      .map((spot) => [spot.getIdOfSpot(), spot.getVehicleAtSpot().getRegnOfVehicle()].join(spacer));

    return [...data, ...tmpArr].join('\n');
  }
};

module.exports = status;
