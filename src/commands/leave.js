const Parking = require('../classes/parking');

/**
 * handles leave command
 * @param {*} cmdObj
 */
const leave = async (cmdObj) => {
  const { regn, duration } = cmdObj;
  try {
    const res = Parking.leave(regn, parseInt(duration));
    return res
      ? `Registration number ${res.regn} with Slot Number ${res.spotId} is free with Charge ${res.bill}`
      : `Registration number ${regn} not found`;
  } catch (e) {
    throw e;
  }
};

module.exports = leave;
